package br.com.itau.Pedido.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name="pagamento")
public interface PagamentoClient {

	@PostMapping("/{id}")
	public ResponseEntity solicitarPagamento(@PathVariable Integer id);
}
