package br.com.itau.Pedido.models;

import java.time.LocalDateTime;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Pedido {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idPedido;
	
	@NotBlank
	private String nomeCliente;

	@NotBlank
	private String nomeCurso;
	
	@NotNull
	private double precoPago;

	@NotNull
	private boolean statusPagamento;
	
	@NotNull
	private LocalDateTime dataEfetivacaoPagamento;
	
	public int getIdPedido() {
		return idPedido;
	}
	public void setIdPedido(int idPedido) {
		this.idPedido = idPedido;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getNomeCurso() {
		return nomeCurso;
	}
	public void setNomeCurso(String nomeCurso) {
		this.nomeCurso = nomeCurso;
	}
	public double getPrecoPago() {
		return precoPago;
	}
	public void setPrecoPago(double precoPago) {
		this.precoPago = precoPago;
	}
	public boolean isStatusPagamento() {
		return statusPagamento;
	}
	public void setStatusPagamento(boolean statusPagamento) {
		this.statusPagamento = statusPagamento;
	}
	public LocalDateTime getDataEfetivacaoPagamento() {
		return dataEfetivacaoPagamento;
	}
	public void setDataEfetivacaoPagamento(LocalDateTime dataEfetivacaoPagamento) {
		this.dataEfetivacaoPagamento = dataEfetivacaoPagamento;
	}
}
