package br.com.itau.Pedido.Controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.itau.Pedido.models.Pedido;
import br.com.itau.Pedido.services.PedidoService;
import br.com.itau.Pedido.viewobjects.Pagamento;
import br.com.itau.Pedido.viewobjects.PedidoCursoCliente;

@RestController
@RequestMapping("/pedido")
public class PedidoController {
	
	@Autowired
	PedidoService pedidoService;

	@PostMapping
	public ResponseEntity pedidoCliente(@RequestBody PedidoCursoCliente pedidoCliente) {
		Pedido pedido = pedidoService.efetuarPedido(pedidoCliente);
		
		if (pedido.getIdPedido() == 0) {
			return ResponseEntity.status(400).build(); //mensagem de curso indisponivel
		}
		return ResponseEntity.ok(pedido);
	}
	
	@PostMapping("/atualizar")
	public ResponseEntity atualizarPagamento(@RequestBody Pagamento pagamento) {
		Pedido pedido = pedidoService.atualizar(pagamento);
		return ResponseEntity.ok(pedido);
	}
	
	/*public ResponseEntity pedidoCliente(@RequestBody PedidoCursoCliente pedidoCliente) {
		boolean pedido = pedidoService.efetuarPedido(pedidoCliente);
		
		if (pedido) {
			return ResponseEntity.ok().build();
		}
		return ResponseEntity.status(400).build();
	}*/
	
	//consulta pedido, pra ver se status esta ok
	
	//a rotina que vai ler a fila em looping, vai solicitar a gravacao do status final
}
