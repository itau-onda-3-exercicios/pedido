package br.com.itau.Pedido.services;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.itau.Pedido.clients.CursoClient;
import br.com.itau.Pedido.clients.PagamentoClient;
import br.com.itau.Pedido.models.Pedido;
import br.com.itau.Pedido.repositories.PedidoRepository;
import br.com.itau.Pedido.viewobjects.Curso;
import br.com.itau.Pedido.viewobjects.Pagamento;
import br.com.itau.Pedido.viewobjects.PedidoCursoCliente;

@Service
public class PedidoService {
	
	private boolean statusDisponibilidadeCurso;

	@Autowired
	PedidoRepository pedidoRepository;

	@Autowired
	CursoClient cursoClient;
	
	@Autowired
	PagamentoClient pagamentoClient;
	
	Curso curso = new Curso();
	
	public Pedido atualizar(Pagamento pagamento) {
		Optional<Pedido> obj = pedidoRepository.findById(pagamento.getId());
		Pedido pedido = obj.orElse(null);
		
		pedido.setStatusPagamento(pagamento.getStatus());
		
		pedidoRepository.save(pedido);
		return pedido;
	}
	
	public Pedido efetuarPedido(PedidoCursoCliente pedidoCliente) {
		Pedido pedido = new Pedido();
		statusDisponibilidadeCurso = obterDadosDoCurso(pedidoCliente.getNomeCurso());
		
		if (statusDisponibilidadeCurso) {
			pedido = gravarPedido(pedidoCliente);
			solicitarPagamento(pedido.getIdPedido());
		}
		return pedido;
	}
	
	private Pedido gravarPedido(PedidoCursoCliente pedidoCliente) {
		Pedido pedido = new Pedido();

		pedido.setPrecoPago(curso.getPreco());
		pedido.setNomeCliente(pedidoCliente.getNomeCliente());
		pedido.setNomeCurso(pedidoCliente.getNomeCurso());
		pedido.setStatusPagamento(false);
		pedido.setDataEfetivacaoPagamento(LocalDateTime.now());
		
		pedidoRepository.save(pedido);
		return pedido;
	}
	
	private void solicitarPagamento(Integer id) {
		pagamentoClient.solicitarPagamento(id);
	}

	private boolean obterDadosDoCurso(String nomeCurso){
		
		Curso curso1 = cursoClient.buscarCursoPorNome(nomeCurso);
		
		if (curso1.isDisponivel()){
			curso.setPreco(curso1.getPreco());
		}
		return curso1.isDisponivel();
	}
}